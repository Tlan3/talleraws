const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const app = express()
const config = require('./config')

app.use(bodyParser.json())


function response (res, code, obj){
    res.statusCode = code
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify(obj))
}

app.post("/grantingToken", (req, res) => {
    let user = req.body.user
    if(user !== undefined){
        let server_token = jwt.sign(user, config.jwt.secret)
        response(res, 200,{message: "Bienvenido", server_token : server_token})
    }
    else{
        response(res, 401,{message: "El usuario no ha sido proporcionado"})
    }
})

app.get("/auth", (req, res) => {
    let server_token = req.headers['authorization']
    if (server_token) {
        jwt.verify(server_token, config.jwt.secret, {ignoreExpiration: true}, (err, decoded) => {
            if (err) {
                response(res, 401,{message: "Token inválido! :/"})
            } else {
                response(res, 200,{message: "token válido", user: decoded})
            }
        })
    } else {
        response(res, 401,{message: "No se ha recibido token! :/"})
    }
})


app.listen(3000, function() {
    console.log(`Servidor arriba`);
})